﻿using System.Linq;
using Windows.UI.Xaml.Controls;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace TestCurrencyConverter.Views
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainView: Page
    {
        public MainView()
        {
            this.InitializeComponent();
            this.RightValue.BeforeTextChanging += BeforeTextChanging;
            this.LeftValue.BeforeTextChanging += BeforeTextChanging;
        }

        private void BeforeTextChanging(TextBox sender, TextBoxBeforeTextChangingEventArgs args)
        {
            args.Cancel = !args.NewText.All(c => char.IsDigit(c) || ','.Equals(c)) ||
                args.NewText.Count(c => ','.Equals(c)) > 1;
        }
    }
}
