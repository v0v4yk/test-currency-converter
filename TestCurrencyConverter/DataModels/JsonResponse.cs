﻿using System;
using System.Collections.Generic;

namespace TestCurrencyConverter.DataModels
{
    public class JsonResponse
    {
        private DateTime _date;
        public string Date { get => _date.ToString("s"); 
            set 
            { 
                if (!DateTime.TryParse(value, out var _date))
                {
                    throw new Exception("Wrong DateTime");
                }
            }
        }
        public string PreviousDate { get; set; }
        public string PreviousUrl { get; set; }
        public string TimeStamp { get; set; }
        public Dictionary<string, Valute> Valute { get; set; }   
    }
}
