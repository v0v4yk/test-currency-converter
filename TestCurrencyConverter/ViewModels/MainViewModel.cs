﻿using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using TestCurrencyConverter.Api;
using Newtonsoft.Json;
using TestCurrencyConverter.DataModels;
using Windows.UI.Xaml;

namespace TestCurrencyConverter.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        protected readonly INavigationService PageNavigationService;
        private bool CanConvert => LeftSelectedValute != null && RightSelectedValute != null;
        private Valute _leftSelectedValute;
        private Valute _rightSelectedValute;
        private decimal leftValue = 1;
        private decimal rightValue = 1;
        private readonly RestApi Client;
        public MainViewModel(INavigationService pageNavigationService): base(pageNavigationService)
        {
            PageNavigationService = pageNavigationService;
            Client = new RestApi("https://www.cbr-xml-daily.ru/");
            var Rub = new Valute
            {
                CharCode = "RUB",
                ID = "1",
                Name = "Российский рубль",
                Nominal = 1,
                NumCode = 1,
                Value = 1,
                Previous = 1
            };
            Valutes.Add(Rub);
            Load();
        }
        #region Valutes
        public IObservableCollection<Valute> Valutes { get; } = new BindableCollection<Valute>();
        public string LeftValuteCharCode => LeftSelectedValute?.CharCode;
        public string LeftValute
        {
            get => leftValue.ToString();
            set
            {
                if (decimal.TryParse(value, out var result) && result > 0)
                {
                    leftValue = result;
                    ConvertRightValue();
                }
            }
        }
        public Valute LeftSelectedValute { get => _leftSelectedValute;
            set
            {
                _leftSelectedValute = value;
                ConvertLeftValue();
                NotifyOfPropertyChange(nameof(LeftValuteCharCode));
            }
        }

        public string RightValuteCharCode => RightSelectedValute?.CharCode;
        public string RightValute
        {
            get => rightValue.ToString();
            set
            {
                if (decimal.TryParse(value, out var result) && result > 0)
                {
                    rightValue = result;
                    ConvertLeftValue();
                }
            }
        }
        public Valute RightSelectedValute { get => _rightSelectedValute;
            set
            {
                _rightSelectedValute = value;
                ConvertRightValue();
                NotifyOfPropertyChange(nameof(RightValuteCharCode));
            }
        }
        private void ConvertRightValue()
        {
            if (CanConvert)
                rightValue = Convert(leftValue, RightSelectedValute.Value / LeftSelectedValute.Value);
            NotifyOfPropertyChange(nameof(RightValute));
        }
        private void ConvertLeftValue()
        {
            if (CanConvert)
                leftValue = Convert(rightValue, LeftSelectedValute.Value / RightSelectedValute.Value);
            NotifyOfPropertyChange(nameof(LeftValute));
        }
        #endregion

        #region Load
        public string LoadText { get; set; }
        private bool _canLoad = false;
        public bool CanLoad
        {
            get => _canLoad;
            set
            {
                _canLoad = value;
                NotifyOfPropertyChange(nameof(CanLoad));
            }
        }
        private Visibility _loadVisibility = Visibility.Visible;
        public Visibility LoadVisibility
        {
            get => _loadVisibility;
            set
            {
                _loadVisibility = value;
                NotifyOfPropertyChange(nameof(LoadVisibility));
            }
        }
        public bool Loaded { get; set; } = false;
        public void Load()
        {
            CanLoad = false;
            LoadText = "Загрузка...";
            NotifyOfPropertyChange(nameof(LoadText));
            Task.Factory.StartNew(() =>
            {
                var response = Client.GetDailyJson("daily_json.js");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Valutes.AddRange(JsonConvert.DeserializeObject<JsonResponse>(response.Content).Valute.Values);
                    Loaded = true;
                    LoadVisibility = Visibility.Collapsed;
                }
                else
                {
                    LoadText = "Не удалось подключиться к серверу";
                    NotifyOfPropertyChange(nameof(LoadText));
                    CanLoad = true;
                }
            });
            LeftSelectedValute = Valutes.First();
            RightSelectedValute = Valutes.First();
        }
        #endregion

        public decimal Convert(decimal value, decimal coeff) => value / coeff;
    }
}
