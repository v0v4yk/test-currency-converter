﻿using RestSharp;

namespace TestCurrencyConverter.Api
{
    class RestApi
    {
        private RestClient Client;
        private readonly string BaseUrl;

        public RestApi(string Url)
        {
            Client = new RestClient(Url);
            Client.Timeout = 5000;
            Client.ReadWriteTimeout = 5000;

        }
        private IRestRequest CreateRequest(string subUrl, Method method)
        {
            var request = new RestRequest(BaseUrl + subUrl, method);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            return request;
        }
        public IRestResponse GetDailyJson(string url)
        {
            var request = CreateRequest(url, Method.GET);
            var response = Client.Execute(request, Method.GET);
            return response;
        }

    }
}
